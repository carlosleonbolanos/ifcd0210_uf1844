<?php
     class COCHES {
          public $lista;
          
          public function __CONSTRUCT()
          {
               $this->lista = json_decode(file_get_contents("inc/json/coches.json"));
          }

          public function __TOSTRING()
          {
               return "<pre>" . var_export($this->lista, true) . "</pre>";
          }
     }
?>