<form action="<?php echo $home; ?>" method="get">
     <div class="cuerpo">
          <?php
               if (!isset($_GET["query"])) $_GET["query"] = "all";
               $datos = null;
          ?>
          <div class="grid listado">
               <input type="hidden" name="query" value="<?php echo $_GET["query"]; ?>">
               <?php
                    foreach ($coches->lista as $casa) {
                         if ($_GET["query"] != "all" && $casa->marca == $_GET["query"]) {
                              $i = 0;
                              foreach($casa->modelos as $modelo) {
                                   if (isset($_GET["num"]) && $i == $_GET["num"]) $datos = $modelo;
               ?>
               <div class="fila">
                    <div class="elemento marca"><img src="<?php echo "$home/public/img/" . $_GET["query"] . "/logo.png"; ?>" alt="<?php echo $_GET["query"]; ?>"></div>
                    <div class="elemento modelo"><?php echo $modelo->modelo; ?></div>
                    <div class="elemento ver"><button type="submit" name="num" value="<?php echo $i; ?>">Ficha</button></div>
               </div>
               <?php
                                   $i++;
                              }
                         }
                         elseif ($_GET["query"] == "all") {
                              $i = 0;
                              foreach($casa->modelos as $modelo) {
               ?>
               <div class="fila">
                    <div class="elemento marca"><img src="<?php echo "$home/public/img/" . $casa->marca . "/logo.png"; ?>" alt="<?php echo $casa->marca; ?>"></div>
                    <div class="elemento modelo"><?php echo $modelo->modelo; ?></div>
                    <div class="elemento ver"><button type="button" onclick="location.href='<?php echo $home . "/?query=$casa->marca&num=$i"; ?>'">Ficha</button></div>
               </div>
               <?php
                                   $i++;
                              }
                         }
                    }
               ?>
          </div>
          <div class="grid imagen">
               <?php
                    switch(true) :
                         case (!isset($_GET["query"])) :
               ?>
               <img src="<?php echo $home; ?>/public/img/www.png" alt="Logo">
               <?php
                         break;
                         case($_GET["query"] != "all" && !isset($_GET["num"])) :
               ?>
               <img src="<?php echo "$home/public/img/" . $_GET["query"] . "/logo.png"; ?>" alt="<?php echo $_GET["query"]; ?>">
               <?php
                         break;
                         case($_GET["query"] != "all" && isset($_GET["num"])) :
               ?>
               <img src="<?php echo "$home/public/img/" . $_GET["query"] . "/$datos->imagen"; ?>" alt="<?php echo $datos->modelo; ?>">
               <?php
                         break;
                         case($_GET["query"] == "all" ) :
               ?>
               <img src="<?php echo $home; ?>/public/img/Audi/logo.png" alt="Audi">
               <img src="<?php echo $home; ?>/public/img/Citroen/logo.png" alt="Citroen">
               <img src="<?php echo $home; ?>/public/img/Dacia/logo.png" alt="Dacia">
               <img src="<?php echo $home; ?>/public/img/Lamborghini/logo.png" alt="Lamborghini">
               <?php
                    endswitch;
               ?>
          </div>
          <div class="grid ficha">
               <?php if (isset($datos)) : ?>
               <div class="subelemento">Modelo:</div>
               <div class="subelemento"><?php echo $datos->modelo; ?></div>
               <div class="subelemento">Inicio de Fabricación:</div>
               <div class="subelemento"><?php echo $datos->announo; ?></div>
               <div class="subelemento">Fin de Fabricación:</div>
               <div class="subelemento"><?php echo $datos->annofin; ?></div>
               <div class="subelemento">Tipo de Motor / Combustible:</div>
               <div class="subelemento"><?php echo $datos->tipo; ?></div>
               <div class="subelemento">Potencia del Motor:</div>
               <div class="subelemento"><?php echo $datos->potencia; ?></div>
               <div class="subelemento">Cilindrada:</div>
               <div class="subelemento"><?php echo $datos->cilindrada; ?></div>
               <div class="subelemento">Tipo cambio:</div>
               <div class="subelemento"><?php echo $datos->cambio; ?></div>
               <?php endif; ?>
          </div>
     </div>
</form>