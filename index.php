<?php
     require_once ("inc/model/json.php");
     $coches = new COCHES;
     $home = $_SERVER["REQUEST_SCHEME"] . "://" . $_SERVER["SERVER_NAME"] . substr($_SERVER["SCRIPT_NAME"],0,strrpos($_SERVER["SCRIPT_NAME"],"/"));
?>
<!DOCTYPE html>
<html lang="es">
<head>
     <meta charset="UTF-8">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <title>Carlos León Bolaños - Examen UF1844</title>
     <style>
          @import "<?php echo $home; ?>/public/css/colors.css";
          @import "<?php echo $home; ?>/public/css/fonts.css";
          @import "<?php echo $home; ?>/public/css/colors.css";
          @import "<?php echo $home; ?>/public/css/common.css";
          @import "<?php echo $home; ?>/public/css/index.css";
          @import "<?php echo $home; ?>/public/css/header.css";
          @import "<?php echo $home; ?>/public/css/footer.css";
     </style>
</head>
<body>
     <?php include_once ("inc/view/header.php"); ?>
     <?php require_once ("inc/view/body.php"); ?>
     <?php include_once ("inc/view/footer.php"); ?>
</body>
</html>